import asyncio

from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling

import json # Para transformar el json a objetos Python
import sdp_transform
import asyncio


class UDPSignalling:
    sdp = None
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        UDPSignalling.sdp = data.decode()
        print("Close the socket")
        self.transport.close()

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def register():
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()
    message = "Register Server"

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: UDPSignalling(message, on_con_lost),
        remote_addr=('127.0.0.1', 9999))

    try:
        await on_con_lost
    finally:
        transport.close()



async def sdp(message):
    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: UDPSignalling(message, on_con_lost),
        remote_addr=('127.0.0.1', 9999))

    try:
        await on_con_lost
    finally:
        transport.close()




time_start = None


async def run_answer(pc, signaling):
    await signaling

    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")

        @channel.on("message")
        def on_message(message):

            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                # reply
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)
    #await consume_signaling(pc)
    await sdp("Esperando SDP Cliente...")
    dict = json.loads(UDPSignalling.sdp)
    sdp_str = RTCSessionDescription(dict, type='offer')
    await pc.setRemoteDescription(sdp_str)

    await  pc.setLocalDescription(await pc.createAnswer())
    message = {"sdp": pc.localDescription.sdp, "type": pc.localDescription.type}
    message = json.dumps(message)
    await sdp(message)



if __name__ == "__main__":
    signaling = register()
    pc = RTCPeerConnection()
    coro = run_answer(pc, signaling)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        signaling = CopyAndPasteSignaling()
        loop.run_until_complete(signaling.close())
