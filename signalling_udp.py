import asyncio
import time
import json
from asyncio import wait
import sdp_transform
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling

class UDPSignalling:
    registers = {"Register Client": None, "Register Server": None}
    sdpClient = None
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        if message == "Register Client":
            print('Received %r from %s' % (message, addr))
            UDPSignalling.registers[message] = addr
            data = "OK"
            print('Send %r to %s' % ("OK", addr))
            self.transport.sendto(data.encode(), addr)

        elif message == "Register Server":
            print('Received %r from %s' % (message, addr))
            UDPSignalling.registers[message] = addr
            data = "OK"
            print('Send %r to %s' % ("OK", addr))
            self.transport.sendto(data.encode(), addr)

        elif message == "Esperando SDP Cliente...":
            print('Received %r from %s' % (message, addr))
            UDPSignalling.registers["Register Server"] = addr


        elif message == "Esperando SDP Servidor...":
            print('Received %r from %s' % (message, addr))
            UDPSignalling.registers["Register Client"] = addr


        else:
            try:
                message = json.loads(message)
                print("JSON received:", message)
            except KeyboardInterrupt:
                pass
            type = message["type"]
            if type == "offer":
                if UDPSignalling.registers["Register Server"] is None:
                    UDPSignalling.sdpClient = message
                else:
                    sdp = json.dumps(message["sdp"])
                    self.transport.sendto(sdp.encode(), UDPSignalling.registers["Register Server"])
                    print('Send %r to %s' % (message["sdp"], UDPSignalling.registers["Register Server"]))
                    data = "OK"
                    print('Send %r to %s' % ("OK", addr))
                    self.transport.sendto(data.encode(), addr)

            elif type == "answer":
                sdp = json.dumps(message["sdp"])
                self.transport.sendto(sdp.encode(), UDPSignalling.registers["Register Client"])
                print('Send %r to %s' % (message["sdp"], UDPSignalling.registers["Register Client"]))
                data = "OK"
                print('Send %r to %s' % ("OK", addr))
                self.transport.sendto(data.encode(), addr)




async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: UDPSignalling(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())